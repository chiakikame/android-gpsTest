package io.company.no.gpstest

import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle

/*
 * Created by chiaki on 19/09/16.
 */
class GPSListener(
        val locationHandler: (Location) -> Unit,
        val disableHandler: () -> Unit,
        val enableHandler: () -> Unit
): LocationListener {
    override fun onLocationChanged(location: Location?) {
        if (location != null)
            locationHandler(location)
    }

    override fun onProviderDisabled(provider: String?) {
        if (provider == LocationManager.GPS_PROVIDER) {
            disableHandler()
        }
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}

    override fun onProviderEnabled(provider: String?) {
        if (provider == LocationManager.GPS_PROVIDER) {
            enableHandler()
        }
    }
}