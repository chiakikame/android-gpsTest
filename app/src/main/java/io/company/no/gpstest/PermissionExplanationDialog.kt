package io.company.no.gpstest

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog

/*
 * Created by chiaki on 19/09/16.
 */
class PermissionExplanationDialog: DialogFragment() {
    var okHandler: (() -> Unit)? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(activity)
                .setTitle(R.string.permission_title)
                .setMessage(R.string.permission_explanation)
        .setPositiveButton(android.R.string.ok, {dialog, which ->
            okHandler?.invoke()
        }).create()
    }
}