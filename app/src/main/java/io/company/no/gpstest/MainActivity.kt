package io.company.no.gpstest

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.util.Log
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private var locationManager: LocationManager? = null
    private var locationListener: LocationListener? = null
    private val requestCode = 1024

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()
        initGps()
    }

    override fun onStop() {
        super.onStop()
        if (locationListener != null) {
            locationManager?.removeUpdates(locationListener)
            locationListener = null
        }
        locationManager = null
    }

    private fun initGps() {
        if (checkPermission()) {
            if (locationManager == null) {
                locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            }

            if (locationManager?.allProviders?.contains(LocationManager.GPS_PROVIDER) != true) {
                showError(R.string.result_error_no_gps)
            }

            if (locationListener == null) {
                locationListener = GPSListener({ location ->
                    showLocation(location)
                }, { // disable
                    showError(R.string.result_error_disabled)
                    Log.i("gpsApp", "GPS disabled")
                }, {
                    Log.i("gpsApp", "GPS enabled")
                })
                locationManager?.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, locationListener)
                val lastKnown = locationManager?.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                if (lastKnown != null) showLocation(lastKnown)
            }
        } else {
            showError(R.string.result_error_permission_denied)
        }
    }

    private fun checkPermission(): Boolean {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                val dialog = PermissionExplanationDialog()
                dialog.okHandler = {
                    requestPermission()
                }
                dialog.show(supportFragmentManager, "dialog")
            } else {
                requestPermission()
            }
            return false
        }
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), requestCode)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == this.requestCode) {
            if (grantResults.all {PackageManager.PERMISSION_GRANTED == it}) {
                initGps()
            } else {
                showError(R.string.result_error_permission_denied)
            }
        }
    }

    private fun showError(errorMessage: String) {
        val view = findViewById(R.id.textView) as TextView
        val content = StringBuilder()
        content.appendln(resources.getString(R.string.result_error_title))
        content.appendln(errorMessage)
        view.text = content.toString()
    }

    private fun showError(errorMessageResourceId: Int) {
        showError(resources.getString(errorMessageResourceId))
    }

    private fun showLocation(location: Location) {
        val view = findViewById(R.id.textView) as TextView
        val content = StringBuilder()
        val naString = resources.getString(R.string.result_normal_not_available)

        content.appendln(resources.getString(R.string.result_normal_title))

        content.appendln(resources.getString(R.string.result_normal_coordinate))
        content.appendln("${location.latitude}, ${location.longitude}")
        content.appendln()

        content.appendln(resources.getString(R.string.result_normal_bearing))
        if (location.hasBearing()) {
            content.append(location.bearing)
            content.appendln(R.string.result_normal_unit_degree)
        } else {
            content.appendln(naString)
        }
        content.appendln()

        content.appendln(resources.getString(R.string.result_normal_altitude))
        if (location.hasAltitude()) {
            content.append(location.altitude)
            content.appendln(R.string.result_normal_unit_meter)
        } else {
            content.appendln(naString)
        }
        content.appendln()

        view.text = content.toString()
    }
}
